# Fu - File Unificator

File Unificator is a command line utility to store files and folders and get them usings a tag syntax.

Adding a file will scan metadata and move content to inside a `.fu` folder.
Adding a folder will preserve it sub-content and will try to find some metadata out of it.
Metadata scanning add tags to your items but you can still add your own tags.

```
$ mkdir demo
$ cd demo
$ fu init .
$ fu add ~/my_photo.jpeg --tag me,photo,profile
$ fu get photo
```

# License

> Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
>
> This work is open source software, licensed under the terms of the
> Mozilla Public License Version 2.0 as described in the LICENSE file
> in the top-level directory.
