# general
- write a README
- move files collection inside FuEnv

# new commands:
- fu mv: rename files ?
- fu debug-info: produce support information (like OS version, fu version, libraries, date, ...) https://stackoverflow.com/questions/27840394/how-can-a-rust-program-access-metadata-from-its-cargo-package
- fu internal-clean: check for missing items and remove them from Fu storage
- fu search <tag search> list elements (files/folders) matching a tag search without adjusting folder's content or maybe an option in 'fu tag' to avoid checkout ?

# fu init
- write 'version' file in .fu folder containing the current fu version

# fu rm
- make a trash in .fu/trash: trash limited size, auto remove oldest files in trash, config in .fu/config

# fu ls
- combine database requests

# fu get
- add option to cumulate searches (do not wipe root dir)
- flag to search on file name instead of tag
- manage a real query syntax

# fu add
- make sure that .fu folder is not included inside the item to add
- maybe not ignore all .fu ? just detect the root's one -> canonicalize()
- option to not show item in current view after adding it
- launch metadata scanning process in background
- move back data to it's original place in case of fail ?

# db
- separate DB operations and use a Trait
- use transations with ejdb

# storage
- separate File operations and use a Trait

# common
- avoid deprecation warning in soft links
- test on windows

# query syntax
- simple 'and', 'or', '()'
- use negation '!'
- use ~meh to get any files with a tag containing "meh" (like "*meh*")
- use asterix mean: tag with some stars => meh, will be a problem in a shell
- same with '+' for priority
- use comparison: '>', '<', '<=', '>=', '=='. Example: '>4*' ? '>****'

# metadata scanning
- http://www.forensicswiki.org/wiki/Document_Metadata_Extraction
- extension
- mime-type
- name separated by non-[a-Z0-9] characeters
- images: metadata of an image
- mp3/ogg/... : metadata
- archive: get file names
- file size: not stored (stored on fs informations and may vary)

# testing
- focus on functional tests
- make it cross platform => avoid scripting and use rust test suite
- early integration with gitlab CI with auto-publish on crates.io + packaging for different platforms (linux, mac, windows) + doc generation + push doc on gitlab static webpages