// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use clap::ArgMatches;
use std::process::exit;
use std::fs;
use common;
use ejdb::query::{Q, QH};
use std::collections::HashMap;

pub fn ls(args: &ArgMatches) {
    let fu_env = common::FuEnv::new();

    if args.is_present("tags") {
        ls_tags(&fu_env);
        return;
    }

    let dir = match fs::read_dir(fu_env.absolute_root.as_path()) {
        Ok(d) => d,
        Err(_) => {
            println!("fatal: cannot read {} content",
                     fu_env.absolute_root.display());
            exit(1);
        }
    };

    for entry in dir {
        let path = match entry {
            Ok(p) => p.path(),
            Err(_) => continue,
        };

        let filename = match path.file_name() {
            Some(n) => {
                match n.to_str() {
                    Some(s) => s,
                    None => continue,
                }
            }
            None => continue,
        };
        if filename == ".fu" {
            continue;
        }

        let id = match common::pathbuf_to_id(&path) {
            Some(id) => id,
            None => {
                println!("{}: untracked", filename);
                continue;
            }
        };

        print!("{}: ", filename);

        let q = match fu_env.files().query(Q.id(id), QH.empty()).find_one() {
            Ok(q) => q,
            Err(_) => {
                println!("error: metadata query failed");
                continue;
            }
        };
        let results = match q {
            Some(r) => r,
            None => {
                println!("error: file's metadata not found");
                continue;
            }
        };
        let tags = match results.get_array("tags") {
            Ok(t) => t,
            Err(_) => {
                println!("no tag");
                continue;
            }
        };
        for t in tags {
            let t = t.to_json();
            let t = match t.as_string() {
                Some(t) => t,
                None => {
                    continue;
                }
            };
            print!("<{}> ", t);
        }
        println!("");
    }
}

fn ls_tags(fu_env: &common::FuEnv) {
    let query = match fu_env.files()
        .query(Q.field("tags").exists(true), QH.field("tags").include())
        .find() {
        Ok(q) => q,
        Err(_) => {
            println!("fatal: tag list query build failed");
            exit(1);
        }
    };

    let mut all_tags = HashMap::new();
    for r in query {
        let results = match r {
            Ok(r) => r,
            Err(_) => {
                continue;
            }
        };
        let tags = match results.get_array("tags") {
            Ok(t) => t,
            Err(_) => {
                continue;
            }
        };
        for t in tags {
            let t = t.to_json();
            let t = match t.as_string() {
                Some(tag) => String::from(tag),
                None => {
                    continue;
                }
            };
            let cnt = match all_tags.get(&t) {
                Some(&cnt) => cnt,
                None => 0,
            };
            all_tags.insert(t, cnt + 1);
        }
    }

    for (tag, cnt) in all_tags.iter() {
        println!("{}: {}", tag, cnt);
    }

    // Count the number of files without tags
    let q = Q.field("tags").exists(false);
    let count = match fu_env.files().query(q, QH.empty()).count() {
        Ok(r) => r,
        Err(_) => {
            println!("fatal: metadata query build failed");
            exit(1);
        }
    };
    if count > 0 {
        println!("*no tag*: {:?}", count);
    }
}
