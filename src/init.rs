// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::path::Path;
use std::fs::create_dir;
use std::process::exit;
use ejdb::Database;
use clap::ArgMatches;

pub fn init(args: &ArgMatches) {
    let mut path = ".";
    if let Some(p) = args.value_of("path") {
        path = p;
    }

    print!("Initializing Fu in {:?} ... ", path);

    let p = Path::new(path);
    if !p.is_dir() {
        println!("FAILED");
        println!("provided path ({}) is not a valid directory", path);
        exit(1);
    }
    let fu_path = p.join(".fu");

    if fu_path.exists() {
        println!("FAILED");
        println!("fu seems already initialized in {}", path);
        exit(1);
    }

    if create_dir(&fu_path).is_err() {
        println!("FAILED");
        println!("cannot create directory {}", fu_path.display());
        exit(1);
    }

    if create_dir(&fu_path.join("data")).is_err() {
        println!("FAILED");
        println!("cannot create directory {}", fu_path.join("data").display());
        exit(1);
    }

    let db_path = fu_path.join("ejdb_metadata");
    let db_path = match db_path.to_str() {
        Some(p) => p,
        None => {
            println!("FAILED");
            println!("cannot build database path {}",
                     fu_path.join("ejdb_metadata").display());
            exit(1);
        }
    };
    let db = match Database::open(db_path) {
        Ok(db) => db,
        Err(_) => {
            println!("FAILED");
            println!("cannot open database {}",
                     fu_path.join("ejdb_metadata").display());
            exit(1);
        }
    };

    match db.collection("files") {
        Ok(_) => {}
        Err(_) => {
            println!("FAILED");
            println!("cannot open database {}",
                     fu_path.join("ejdb_metadata_files").display());
            exit(1);
        }
    };

    println!("OK");
    println!("Congratulation! move some data in {} and try now to 'fu add' them :)",
             path);
    println!("Run 'fu help' for more information");
}
