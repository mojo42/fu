// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#[macro_use(bson)]
extern crate ejdb;
extern crate clap;
extern crate ini;
extern crate chrono;
extern crate crc;
extern crate rustc_serialize as serialize;
extern crate crypto;
extern crate symlink;

mod init;
mod add;
mod get;
mod common;
mod tag;
mod untag;
mod ls;
mod clear;
mod rm;

use clap::{App, SubCommand, Arg};
use std::process::exit;

fn main() {
    let args = App::new("File Unificator")
        .version("0.1")
        .author("Jerome Jutteau <j.jutteau@gmail.com>")
        .about("File Unificator is an easy way to manage your files using tags")
        .subcommand(SubCommand::with_name("init")
            .about("Create an empty Fu storage")
            .args_from_usage("[PATH] 'Sets an optional path where to init Fu'"))
        .subcommand(SubCommand::with_name("add")
            .about("Add non tracked items to Fu storage")
            .arg(Arg::with_name("path")
                .help("path to file or folder to add")
                .required(true)
                .multiple(true))
            .args_from_usage("-f, --flat 'recusively add files")
            .arg(Arg::with_name("tag")
                .short("-t")
                .long("--tag")
                .help("Add tags to item(s)")
                .use_delimiter(true)
                .takes_value(true)
                .multiple(true)))
        .subcommand(SubCommand::with_name("get")
            .about("Adjust folder content to only show matching items")
            .args_from_usage("-k, --keep 'avoid clearing current view")
            .arg(Arg::with_name("tag")
                .help("combinaison of tags to match")
                .required(false)
                .multiple(true)))
        .subcommand(SubCommand::with_name("tag")
            .about("Tag a file or folder")
            .arg(Arg::with_name("path")
                .help("path to file or folder to add")
                .required(true)
                .multiple(true))
            .arg(Arg::with_name("tag")
                .short("-t")
                .long("--tag")
                .help("Add tags to item(s)")
                .use_delimiter(true)
                .takes_value(true)
                .multiple(true)))
        .subcommand(SubCommand::with_name("untag")
            .about("Remove tags from file(s) or folder(s)")
            .arg(Arg::with_name("path")
                .help("file(s) or folder(s)")
                .required(true)
                .multiple(true))
            .arg(Arg::with_name("tag")
                .short("-t")
                .long("--tag")
                .help("tag to remove")
                .use_delimiter(true)
                .takes_value(true)
                .multiple(true)))
        .subcommand(SubCommand::with_name("ls")
            .about("Show the files and folders status")
            .args_from_usage("-t, --tags 'list all existing tags"))
        .subcommand(SubCommand::with_name("clear").about("Clear current view"))
        .subcommand(SubCommand::with_name("rm")
            .about("Remove file(s) or folder(s) from Fu storage")
            .arg(Arg::with_name("path")
                .help("file(s) or folder(s)")
                .required(true)
                .multiple(true)))
        .get_matches();

    if let Some(args) = args.subcommand_matches("init") {
        init::init(&args);
    } else if let Some(args) = args.subcommand_matches("add") {
        add::add(&args);
    } else if let Some(args) = args.subcommand_matches("get") {
        get::get(&args);
    } else if let Some(args) = args.subcommand_matches("tag") {
        tag::tag(&args);
    } else if let Some(args) = args.subcommand_matches("untag") {
        untag::untag(&args);
    } else if let Some(args) = args.subcommand_matches("rm") {
        rm::rm(&args);
    } else if let Some(args) = args.subcommand_matches("ls") {
        ls::ls(&args);
    } else if args.subcommand_matches("clear").is_some() {
        clear::clear();
    } else {
        println!("no command provided, run fu --help for usage");
        exit(1);
    }
}
