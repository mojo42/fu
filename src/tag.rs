// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use clap::ArgMatches;
use common;

pub fn tag(args: &ArgMatches) {
    let fu_env = common::FuEnv::new();

    let all_path = match args.values_of("path") {
        Some(p) => p,
        None => {
            println!("fatal: cannot read path arguments");
            return;
        }
    };

    for path in all_path {
        let id = match common::path_to_id(&path) {
            Some(i) => i,
            None => {
                println!("{} don't seems to be in Fu, use 'fu add' ?", path);
                continue;
            }
        };

        if let Some(tags) = args.values_of("tag") {
            for tag in tags {
                common::add_tag(&fu_env, &id, tag);
            }
        }
    }
}
