// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::fs;
use std::process::exit;
use std::path::PathBuf;
use clap::ArgMatches;
use common;
use ejdb::query::{Q, QH};

pub fn rm(args: &ArgMatches) {
    let fu_env = common::FuEnv::new();

    let paths = match args.values_of("path") {
        Some(p) => p,
        None => {
            println!("fatal: cannot get path list");
            exit(1);
        }
    };

    'items: for path in paths.map(|p| PathBuf::from(p)) {
        let id = match common::pathbuf_to_id(&path) {
            Some(id) => id,
            None => continue,
        };

        let mut link = match path.read_link() {
            Ok(l) => l,
            Err(_) => continue,
        };

        if fu_env.files().query(Q.id(id).drop_all(), QH.empty()).update().is_err() {
            println!("error while removing {} metadata", path.display());
            continue;
        }

        if fs::remove_file(path.as_path()).is_err() {
            println!("cannot remove symbolic link {}", path.display());
            continue;
        }

        if link.is_dir() {
            if fs::remove_dir_all(link.as_path()).is_err() {
                println!("{}: cannot remove {} folder from storage",
                         path.display(),
                         link.display());
                continue;
            }
        } else {
            if fs::remove_file(link.as_path()).is_err() {
                continue;
            }
        }

        for _ in 0..common::DATA_DEPTH {
            link.pop();
            match link.read_dir() {
                Ok(r) => {
                    if r.count() == 0 {
                        if fs::remove_dir(&link).is_err() {
                            continue 'items;
                        }
                    }
                }
                Err(_) => continue 'items,
            }
        }
    }
}
