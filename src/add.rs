// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::io;
use std::fs::{rename, read_dir};
use std::process::exit;
use std::path::PathBuf;
use clap::ArgMatches;
use common;
use common::FuEnv;
use serialize::hex::ToHex;
use std::fs::create_dir_all;

pub fn add(args: &ArgMatches) {
    let fu_env = common::FuEnv::new();

    if let Some(user_list) = args.values_of("path") {
        for p in user_list {
            let mut path = PathBuf::new();
            path.push(p);

            if args.is_present("flat") && path.is_dir() {
                match add_recurse(&fu_env, &path, &args) {
                    _ => {}
                }
            } else {
                match add_item(&fu_env, &path, &args) {
                    _ => {}
                };
            }
        }
    } else {
        println!("cannot current list directory");
        exit(1);
    }
}

fn add_recurse(fu_env: &FuEnv, dir: &PathBuf, args: &ArgMatches) -> Result<(), io::Error> {
    if dir.is_dir() {
        for entry in read_dir(dir)? {
            let entry = try!(entry);
            let path = entry.path();
            if path.is_dir() {
                add_recurse(&fu_env, &path, &args)?;
            } else {
                add_item(&fu_env, &path, &args)?;
            }
        }
    }
    Ok(())
}

fn add_item(fu_env: &FuEnv, path: &PathBuf, args: &ArgMatches) -> Result<(), io::Error> {
    let filename = match path.file_name() {
        Some(f) => f,
        None => return Err(io::Error::new(io::ErrorKind::Other, "cannot get file name")),
    };
    let filename = match filename.to_str() {
        Some(s) => String::from(s),
        None => return Err(io::Error::new(io::ErrorKind::Other, "cannot get file name")),
    };
    if filename == ".fu" {
        return Err(io::Error::new(io::ErrorKind::Other, "ignore .fu folder"));
    }

    let metadata = path.symlink_metadata()?;
    if metadata.file_type().is_symlink() {
        return Err(io::Error::new(io::ErrorKind::Other, "ignore symbolic links on add"));
    }

    let fna = filename.clone();
    let id = match fu_env.files().save(&bson! {"name" => fna}) {
            Ok(id) => id,
            Err(_) => {
                return Err(io::Error::new(io::ErrorKind::Other, "metadata writing failed"));
            }
        }
        .to_hex();

    if let Some(tags) = args.values_of("tag") {
        for tag in tags {
            common::add_tag(&fu_env, &id, tag);
        }
    }

    let storage_path = common::id_to_path(&id);
    let mut storage_dir = storage_path.clone();
    storage_dir.pop();
    create_dir_all(storage_dir)?;
    rename(path.as_path(), &storage_path)?;
    common::checkout_start(&fu_env);
    common::checkout(&id, &filename);
    common::checkout_end(&fu_env);
    
    // add some metadata
    if let Some(extension) = path.extension() {
        if let Some(extension) = extension.to_str() {
            common::add_tag(&fu_env, &id, extension);
        }
    }

    Ok(())
}
