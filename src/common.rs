// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::path::{PathBuf, Path};
use std::env::current_dir;
use std::env;
use std::process::exit;
use std::fs;
use std::io;
use ejdb::{Database, Collection};
use ejdb::query::{Q, QH};
use crypto::md5::Md5;
use crypto::digest::Digest;
use symlink;

pub const DATA_DEPTH: usize = 2;

pub struct FuEnv {
    pub absolute_root: PathBuf,
    pub workdir: PathBuf,
    pub db: Database,
}

impl FuEnv {
    pub fn new() -> FuEnv {
        FuEnv {
            absolute_root: match root() {
                Some(f) => f,
                None => {
                    println!("fatal: Not a fu storage (or any of the parent storage): .fu");
                    exit(1);
                }
            },
            db: match connect() {
                Some(db) => db,
                None => {
                    println!("fatal: cannot read .fu metadata informations");
                    exit(1);
                }
            },
            workdir: match current_dir() {
                Ok(w) => w,
                Err(_) => {
                    println!("fatal: cannot read current dir information");
                    exit(1);
                }
            },
        }
    }
    pub fn files<'a>(&'a self) -> Collection<'a> {
        match self.db.collection("files") {
            Ok(c) => c,
            Err(_) => {
                println!("fatal: cannot read .fu metadata informations");
                exit(1);
            }
        }
    }
}

pub fn root() -> Option<PathBuf> {
    let mut path = match current_dir() {
        Ok(p) => p,
        Err(_) => {
            return None;
        }
    };

    loop {
        let fu = path.join(".fu");
        if fu.exists() && fu.is_dir() {
            return Some(path);
        }

        let p = path;
        match p.parent() {
            Some(parent) => {
                path = PathBuf::new();
                path.push(parent);
            }
            None => return None,
        };
    }
}

pub fn connect() -> Option<Database> {
    if let Some(root) = root() {
        let db_path = root.join(".fu/ejdb_metadata");
        let db_path = match db_path.to_str() {
            Some(p) => p,
            None => {
                return None;
            }
        };
        match Database::open(db_path) {
            Err(_) => None,
            Ok(db) => Some(db),
        }
    } else {
        return None;
    }
}

pub fn soft_link<P: AsRef<Path>, Q: AsRef<Path>>(src: P, dst: Q) -> io::Result<()> {
    symlink::symlink_auto(src, dst)
}

pub fn pathbuf_to_id(path: &PathBuf) -> Option<String> {
    let link = match path.read_link() {
        Ok(l) => l,
        Err(_) => return None,
    };

    if !link.starts_with(".fu") {
        return None;
    }

    let id = match link.file_name() {
        Some(s) => s,
        None => return None,
    };

    let id = match id.to_str() {
        Some(s) => s,
        None => return None,
    };

    return Some(String::from(id));
}

pub fn path_to_id(path: &str) -> Option<String> {
    let path = PathBuf::from(path);
    pathbuf_to_id(&path)
}

pub fn add_tag(fu_env: &FuEnv, id: &String, tag: &str) {
    if fu_env.files().query(Q.id(id).add_to_set("tags", tag), QH.empty()).update().is_err() {
        println!("failed to insert tag {}", tag);
    }
}

pub fn clear(fu_env: &FuEnv) {
    // first, clean all symbolic links
    let dir = match fs::read_dir(&fu_env.absolute_root.as_path()) {
        Ok(d) => d,
        Err(_) => {
            println!("fatal: cannot read fu's root dir {}",
                     fu_env.absolute_root.display());
            exit(1);
        }
    };

    for item in dir {
        if let Ok(item) = item {
            if pathbuf_to_id(&item.path()).is_some() {
                if fs::remove_file(item.path().as_path()).is_err() {
                    println!("cannot remove symbolic link {}", item.path().display());
                }
            }
        }
    }
}

pub fn checkout_start(fu_env: &FuEnv) {
    if env::set_current_dir(&fu_env.absolute_root).is_err() {
        println!("fatal: cannot move to {:?}", &fu_env.absolute_root);
        exit(1);
    }
}

pub fn id_to_path(id: &String) -> PathBuf {
    let mut md5 = Md5::new();
    md5.input_str(id);
    let h = md5.result_str();
    let mut p = PathBuf::new();
    p.push(".fu/data");
    for i in 0..DATA_DEPTH {
        let chunk = h.chars().skip(i).take(2).collect::<String>();
        p.push(chunk);
    }
    p.push(id.to_string());
    return p.clone();
}

pub fn checkout(id: &String, name: &String) {
    let src = id_to_path(&id);

    if soft_link(&src, name.as_str()).is_ok() {
        return;
    }

    for i in 0..11 {
        let n = format!("{}{}", (0..i).map(|_| "_").collect::<String>(), name);
        if soft_link(&src, n.as_str()).is_ok() {
            return;
        }
    }

    let name = format!("{}_{}", id, name);
    if soft_link(src, name.as_str()).is_ok() {
        return;
    }
    println!("error while creating symbolic link {}", name);
}

pub fn checkout_end(fu_env: &FuEnv) {
    if env::set_current_dir(&fu_env.workdir).is_err() {
        println!("fatal: cannot move to {:?}", &fu_env.workdir);
        exit(1);
    }
}
