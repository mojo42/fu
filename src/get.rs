// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use clap::ArgMatches;
use common;
use std::env;
use std::process::exit;
use common::FuEnv;
use ejdb::query::{Q, QH};
use ejdb::query::Query;
use serialize::hex::ToHex;

pub fn get(args: &ArgMatches) {
    let fu_env = common::FuEnv::new();
    if !args.is_present("keep") {
        common::clear(&fu_env);
    }

    common::checkout_start(&fu_env);

    // Move to root dir to build symbolic links
    if env::set_current_dir(&fu_env.absolute_root).is_err() {
        println!("fatal: cannot move to {:?}", &fu_env.absolute_root);
        exit(1);
    }

    if let Some(tags) = args.values_of("tag") {
        // only manage "and"
        let mut v = Vec::new();
        for tag in tags {
            v.push(Q.field("tags").eq(tag));
        }
        let q = Q.and(v);
        checkout_query(&fu_env, &q);
    } else {
        // select items with no tag
        let q = Q.field("tags").exists(false);
        checkout_query(&fu_env, &q);
    }
    common::checkout_end(&fu_env);
}

fn checkout_query(fu_env: &FuEnv, q: &Query) {
    let results = match fu_env.files().query(q, QH.empty()).find() {
        Ok(query) => query,
        Err(_) => {
            println!("fatal: metadata query build failed");
            exit(1);
        }
    };

    for r in results {
        let r = match r {
            Ok(r) => r,
            Err(_) => {
                println!("fatal: metadata query failed for a file");
                exit(1);
            }
        };
        let id = match r.get_object_id("_id") {
            Ok(r) => r.to_hex(),
            Err(_) => {
                println!("fatal: metadata query failed to get file id");
                exit(1);
            }
        };
        let name = match r.get_str("name") {
            Ok(r) => String::from(r),
            Err(_) => {
                println!("fatal: metadata query failed to get file name");
                exit(1);
            }
        };
        common::checkout(&id, &name);
    }
}
