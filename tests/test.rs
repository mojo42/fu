// Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

extern crate tempdir;

use std::env::set_current_dir;
use tempdir::TempDir;
use std::env::current_exe;
use std::process::{Command, Stdio};
use std::fs::{read_dir, create_dir_all, File};
use std::io::Write;


macro_rules! fu_output {
    ( $($arg:expr),* ) => (
        {
            let mut p = current_exe().unwrap();
            p.pop();
            p.pop();
            p.push("fu");
            Command::new(&p) $(.arg($arg))* .output().unwrap();
        }
    );
}

macro_rules! fu {
    ( $($arg:expr),* ) => (
        {
            let mut p = current_exe().unwrap();
            p.pop();
            p.pop();
            p.push("fu");
            assert!(Command::new(&p)
                $(.arg($arg))*
                .stdout(Stdio::null())
                .status().unwrap()
                .success())
        }
    );
}

#[test]
fn it_works() {
    fu!("help");
}

#[test]
fn init() {
    let tmp = TempDir::new("init").unwrap();
    set_current_dir(&tmp).unwrap();
    fu!("init");
    read_dir(".fu").unwrap();
}

fn add_file(tmp: &TempDir, p: &str) {
    let f = tmp.path().join(p);
    println!("{:?}", f);
    create_dir_all(f.parent().unwrap()).unwrap();
    let mut file = File::create(f).expect("create temp file");
    writeln!(file, "some data").expect("write temp file");
}

fn exec(cmd: &str) {
    println!("{:?}", Command::new("bash").arg("-c").arg(cmd).output());
}

#[test]
fn add() {
    let tmp = TempDir::new("init").unwrap();
    fu!("init");
    //add_file(&tmp, "a.txt");
    //add_file(&tmp, "b/a.txt");
    //exec("ls /");
//    fu!("sdfsdf");
}
